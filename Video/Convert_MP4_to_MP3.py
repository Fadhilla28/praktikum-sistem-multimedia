import easygui
from moviepy.editor import *
import zipfile
import os

# Memilih beberapa file video
Video_urls = easygui.fileopenbox(multiple=True)

# Membuat direktori sementara untuk menyimpan file mp3
temp_dir = "temp"
os.makedirs(temp_dir, exist_ok=True)

# Mengubah setiap file video menjadi file mp3
for Video_url in Video_urls:
    FileName = Video_url.split('\\')
    Music_Name = FileName[-1]
    Music_Name = Music_Name[:-4]

    video = VideoFileClip(Video_url)
    video.audio.write_audiofile(os.path.join(temp_dir, '{}.mp3'.format(Music_Name))) #Convert MP4 to MP3

# Membuat file zip
zip_file = zipfile.ZipFile('result.zip', 'w', zipfile.ZIP_DEFLATED)
for root, _, files in os.walk(temp_dir):
    for file in files:
        zip_file.write(os.path.join(root, file), file)
zip_file.close()