# Praktikum Sistem Multimedia

# Image Processing
# No 1
###### Repo: https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/tree/main/Image
# No 2
###### Program image yang saya buat adalah konversi jpg ke pdf, dimana pada saat gambar diupload, maka akan dicek terlebih dahulu apakah gambar orientasinya landscape atau tidak, apabila gambar landscape maka image akan diproses kompresi terlebih dahulu dengan merotasinya menjadi potrait, dan apabila sudah potrait maka akan diproses konversi menjadi pdf dari gambar jpg yang telah diinputkan. Library python yang digunakan :
###### 1. FPDF -> untuk konversi menjadi PDF.
###### 2. PIL -> untuk proses image compression atau image manipulation.
###### 3. os -> untuk melakukan operasi pada sistem operasi, seperti mengakses file dan direktori.
###### Algoritma program :
```mermaid
---
title: Image Processing (JPG to PDF)
---
flowchart TD
    A[Input] -->|JPG| C{is image landscape?}
    C -->|yes| D[Image Compression]
    C -->|no| E[Image Conversion]
    D --> E
    E --> F[Output PDF]
```
Berikut demo programnya :
![demo-image](https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/raw/main/Image/Image.gif)
###### Link Youtube : https://youtu.be/PZeTJSzu5qg
# No 3
###### Aspek kecerdasan buatan dalam program image processing ini terletak pada penggunaan pustaka dan algoritma untuk mengotomatisasi proses konversi beberapa file gambar dalam format JPG menjadi file PDF. Berikut adalah beberapa aspek kecerdasan buatan yang terkait dengan kodingan tersebut:
###### 1. Interaksi Pengguna: Kodingan menggunakan komentar yang menjelaskan input pengguna yang diperlukan. Pengguna diminta untuk memasukkan folder yang berisi gambar-gambar yang akan dikonversi menjadi file PDF. Aspek kecerdasan buatan terkait adalah kemampuan kodingan untuk berinteraksi dengan pengguna dan meminta input yang diperlukan.
###### 2. Pemrosesan Data: Kodingan menggunakan pustaka fpdf dan PIL untuk memproses data gambar dan membuat file PDF. Pustaka fpdf digunakan untuk membuat halaman PDF dan menyimpannya dalam file, sementara PIL digunakan untuk membuka, memanipulasi, dan menyimpan gambar dalam format yang sesuai. Aspek kecerdasan buatan terkait adalah kemampuan kodingan untuk memahami dan memproses data gambar dengan menggunakan algoritma dan pustaka yang sesuai.
###### 3. Manajemen File: Kodingan mengelola file-file yang terlibat dalam proses konversi. Ini mencakup membaca file gambar dari folder yang ditentukan, mengurutkannya berdasarkan nama, mengubah orientasi gambar jika diperlukan, menyimpan gambar yang telah diubah, dan akhirnya menyimpan hasilnya dalam file PDF. Aspek kecerdasan buatan terkait adalah kemampuan kodingan untuk memanipulasi file secara efisien.
###### 4. Automatisasi Proses: Kodingan mengotomatisasi proses konversi file gambar menjadi file PDF. Setelah mengumpulkan daftar semua file gambar dalam folder, kodingan secara otomatis mengubah orientasi gambar dalam mode landscape, jika ada, sehingga hasil PDF memiliki orientasi yang konsisten. Kemudian, kodingan secara otomatis menambahkan setiap gambar sebagai halaman dalam file PDF yang dihasilkan. Aspek kecerdasan buatan terkait adalah kemampuan kodingan untuk mengotomatisasi langkah-langkah tersebut dan menghasilkan hasil akhir yang diinginkan.
###### Secara keseluruhan, image processing ini memanfaatkan pustaka dan algoritma dalam konteks kecerdasan buatan untuk mengotomatisasi proses konversi beberapa file gambar menjadi file PDF. Dengan menggabungkan interaksi pengguna, pemrosesan data, manajemen file, dan automatisasi proses, kodingan tersebut menyediakan solusi yang efisien untuk tugas tertentu, yaitu konversi file gambar menjadi file PDF.

# Audio Processing
# No 1
###### Repo: https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/tree/main/Audio
# No 2
###### Program audio yang saya buat adalah beat detection, dimana program melakukan analisis spektral pada file audio WAV dan mencoba mendeteksi ketukan (beat) dalam file tersebut dengan memeriksa energi dalam berbagai BPM (Beat Per Minute) menggunakan Transformasi Fourier yang dimana ini merupakan suatu metode matematis yang digunakan untuk menganalisis sinyal dalam domain frekuensi. Metode ini mengubah sinyal dari domain waktu menjadi domain frekuensi, sehingga memungkinkan kita untuk melihat komponen-komponen frekuensi yang ada dalam suatu sinyal. Program ini dibuat menggunakan bahasa pemrograman python. Library python yang digunakan :
###### 1. scipy -> untuk membaca file audio WAV.
###### 2. numpy -> untuk manipulasi array numerik.
###### 3. matplotlib -> untuk visualisasi dimana menampilkan hasil analisis spektral dan menampilkan hasil BPM dari wav file.
###### Algoritma program :
```mermaid
---
title: Audio Processing (Beat Detection)
---
flowchart TD
    A[input audio] --> |wav| B(audio compression)
    B --> C(downmix and normalization)
    C --> D(show graphic spectral)
    D --> E(audio compression)
    E --> F(transformasi fourier)
    F --> G(output graphic BPM)
```
###### Berikut demo programnya :
![demo-image](https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/raw/main/Audio/Audio.gif)

###### Link Youtube : https://youtu.be/RnXikcZBNQ4
# No 3
###### Aspek kecerdasan dalam kodingan di atas adalah pemrosesan sinyal audio menggunakan teknik analisis spektral dan deteksi ketukan (beat detection) berdasarkan energi frekuensi dalam sinyal. Berikut adalah beberapa aspek kecerdasan yang terkait dengan program audio processing yang dibuat:
###### 1. Pemrosesan Sinyal Audio: Kodingan tersebut menggunakan modul scipy.io untuk membaca file audio WAV dan mengambil sinyal audio sebagai input. Kemudian, dengan menggunakan modul numpy, dilakukan pemrosesan sinyal audio, seperti downmixing (mengubah sinyal stereo menjadi mono) dan normalisasi sinyal.
###### 2. Analisis Spektral: Kodingan tersebut menggunakan Transformasi Fourier (FFT) untuk mengubah sinyal audio ke domain frekuensi. FFT digunakan untuk menghitung spektrum frekuensi dari sampel audio. Dalam konteks ini, FFT digunakan untuk menghitung spektrum frekuensi dari sinyal audio yang dipilih.
###### 3. Deteksi Ketukan (Beat Detection): Kodingan tersebut mencoba mendeteksi ketukan dalam sinyal audio dengan menganalisis energi dalam berbagai BPM (Beat Per Minute). Melalui perhitungan energi dengan mengalikan komponen frekuensi dari sinyal dengan komponen frekuensi yang berkaitan dengan BPM tertentu, kodingan ini mencoba mengidentifikasi BPM dengan energi maksimum, yang kemungkinan mengindikasikan keberadaan ketukan dalam sinyal audio.
###### 4. Visualisasi Data: Kodingan tersebut menggunakan modul matplotlib untuk membuat grafik yang menampilkan energi BPM terhadap BPM. Dengan menggunakan label sumbu dan tanda merah pada titik dengan energi maksimum, kodingan ini memberikan visualisasi yang memudahkan pemahaman hasil analisis spektral dan deteksi ketukan.
###### Secara keseluruhan, program ini menggabungkan teknik analisis spektral, pemrosesan sinyal audio, dan deteksi ketukan untuk menganalisis sinyal audio dan memberikan pemahaman terhadap energi dan ketukan dalam sinyal tersebut.

# Video Processing
# No 1
###### https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/tree/main/Video
# No 2
###### Program video yang saya buat adalah konversi mp4 ke mp3, dimana inputan berupa video dengan format mp4 yang bisa lebih dari satu file, kemudian akan dikonversi menjadi audio dengan format mp3 menggunakan library moviepy serta selanjutnya hasil yang telah dikonversi akan dikompresi menjadi zip menggunakan library zipfile. Library python yang digunakan :
###### 1. Easygui -> untuk memungkinkan pengguna memilih file video yang akan dikonversi melalui dialog file.
###### 2. Moviepy -> untuk memanipulasi file video dan audio (proses konversi).
###### 3. zipfile -> untuk mengkompresi file output menjadi zip.
###### 4. os -> untuk melakukan operasi pada sistem operasi, sepert membuat folder sementara pada direktori.

```mermaid
---
title: Video Processing (mp4 to mp3 conversion)
---
flowchart TD
    A[input video] --> |mp4| B(video conversion)
    B --> |using moviepy library| C(output audio mp3)
    C --> D(compression)
    D --> |using zipfile library| E(output zip) 
```
Berikut demo programnya :
![demo-image](https://gitlab.com/Fadhilla28/praktikum-sistem-multimedia/-/raw/main/Video/Video.gif)
###### Link Youtube : https://youtu.be/HxARd6d3g2Q
# No 3
###### Dalam video processing ini, ada beberapa aspek kecerdasan buatan yang digunakan:
###### 1. Penggunaan Perpustakaan ("Library") Easygui: Easygui adalah perpustakaan Python yang menyediakan antarmuka grafis sederhana untuk memilih beberapa file video dari sistem file pengguna. Ini memungkinkan interaksi yang lebih intuitif daripada menggunakan antarmuka baris perintah standar.
###### 2. Pengolahan Video dengan Moviepy: Moviepy adalah perpustakaan Python untuk manipulasi video. Dalam skrip ini, perpustakaan digunakan untuk membaca file video yang dipilih oleh pengguna dan mengonversinya menjadi file audio dalam format MP3. Ini dilakukan dengan mengambil audio dari setiap klip video menggunakan metode `write_audiofile` dari objek `video.audio`.
###### 3. Pemrosesan File dengan Modul Zipfile: Modul `zipfile` digunakan untuk membuat file ZIP yang berisi file audio MP3 yang dihasilkan. Setelah konversi file video ke MP3, semua file MP3 disimpan dalam direktori sementara `temp_dir`. Kemudian, skrip berjalan melalui setiap file dalam direktori tersebut dan menambahkannya ke file ZIP menggunakan metode `write` dari objek `zip_file`.

###### Secara keseluruhan, program ini menggunakan kecerdasan buatan untuk membantu pengguna dalam memilih file video, mengonversinya menjadi file audio, dan mengemasnya dalam file ZIP. Ini memanfaatkan kemampuan pemrosesan video dan file Python untuk menyederhanakan tugas-tugas ini dan memberikan pengalaman yang lebih intuitif bagi pengguna.